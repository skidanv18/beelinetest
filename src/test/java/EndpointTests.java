import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class EndpointTests {
    private static final int ALLOWED_HEADERS_COUNT = 92;
    private static final String HOST = "https://httpbin.org/";
    private static final String HEADERS_ENDPOINT = "headers";
    private static final String STATUS_ENDPOINT = "status/";
    private static final String REDIRECT_ENDPOINT = "redirect/";

    private static RequestSpecification requestSpecification;


    @BeforeClass
    public void setUp() {
        requestSpecification = new RequestSpecBuilder()
                .log(LogDetail.ALL)
                .addFilter(new RequestLoggingFilter())
                .addFilter(new ResponseLoggingFilter())
                .build();
    }

    @BeforeMethod
    public static void loggingStart() {
        log("Test has been started");
    }

    @AfterMethod
    public static void loggingFinish() {
        log("Test has been finished");
    }

    /**
     * Positive case. Should get incoming request headers.
     **/
    @Test
    public static void shouldGetIncomingRequestHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        for (int i = 0; i < ALLOWED_HEADERS_COUNT; i++)
            headers.put("T" + i, "t" + i);

        HashMap<String, String> response = given()
                .spec(requestSpecification)
                .headers(headers)
                .when()
                .get(HOST + HEADERS_ENDPOINT)
                .then().statusCode(200)
                .and().extract().body().jsonPath().getJsonObject("headers");

        for (Map.Entry<String, String> header : headers.entrySet())
            assertEquals(response.get(header.getKey()), header.getValue());
    }

    /**
     * Negative case. Should fail to return amount of incoming request headers more than limit.
     **/
    @Test
    public static void shouldFailToReturnTooManyIncomingRequestHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        for (int i = 0; i < ALLOWED_HEADERS_COUNT + 1; i++)
            headers.put("T" + i, "t" + i);


        String response = given()
                .spec(requestSpecification)
                .headers(headers)
                .when()
                .get(HOST + HEADERS_ENDPOINT)
                .then().statusCode(400)
                .and().extract().body().htmlPath().prettify();

        assertTrue(response.contains("Error parsing headers: 'limit request headers fields'"));
    }

    @DataProvider(name = "validStatusCodes")
    public static Object[][] validStatusCodesDataProvider() {
        return new Object[][]{
                {100}, {101}, {102}, {103},
                {200}, {201}, {202}, {203}, {204}, {205}, {206}, {207}, {208}, {226},
                {300}, {301}, {302}, {303}, {304}, {305}, {306}, {307}, {308},
                {400}, {401}, {402}, {403}, {404}, {405}, {406}, {407}, {408}, {409},
                {410}, {411}, {412}, {413}, {414}, {415}, {416}, {417}, {418}, {419},
                {421}, {422}, {423}, {424}, {425}, {426}, {427}, {428}, {429},
                {431}, {449}, {451}, {499},
                {500}, {501}, {502}, {503}, {504}, {505}, {506}, {507}, {508}, {509},
                {511}, {520}, {521}, {522}, {523}, {524}, {525}, {526}
        };
    }

    /**
     * Positive case. Should return valid status code.
     **/
    @Test(dataProvider = "validStatusCodes")
    public static void shouldReturnValidStatusCode(int statusCode) {
        String statusUrl = HOST + STATUS_ENDPOINT + statusCode;
        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", 1000)
                        .setParam("http.socket.timeout", 1000));


        log("DELETE request");
        given()
                .spec(requestSpecification).config(config)
                .when()
                .delete(statusUrl)
                .then()
                .statusCode(statusCode);

        log("GET request");
        given()
                .spec(requestSpecification)
                .when()
                .get(statusUrl)
                .then()
                .statusCode(statusCode);

        log("PATCH request");
        given()
                .spec(requestSpecification)
                .when()
                .patch(statusUrl)
                .then()
                .statusCode(statusCode);

        log("POST request");
        given()
                .spec(requestSpecification)
                .when()
                .post(statusUrl)
                .then()
                .statusCode(statusCode);

        log("PUT request");
        given()
                .spec(requestSpecification)
                .when()
                .put(statusUrl)
                .then()
                .statusCode(statusCode);
    }

    /**
     * Positive case. Should return one of two valid status codes.
     **/
    @Test
    public static void shouldReturnOneOfValidStatusCodes() {
        log("GET request");
        Response response = given()
                .spec(requestSpecification)
                .when()
                .get(HOST + STATUS_ENDPOINT + "201,202");
        int statusCode = response.getStatusCode();
        assertTrue(statusCode == 201 || statusCode == 202);
    }


    @DataProvider(name = "invalidStatusCodes")
    public static Object[][] invalidStatusCodesDataProvider() {
        return new Object[][]{
                {"130"}, {"230"}, {"330"}, {"430"}, {"530"}, {"1000"}, {"test"}
        };
    }

    /**
     * Negative case. Should fail to return invalid status code.
     **/
    @Test(dataProvider = "invalidStatusCodes")
    public static void shouldFailToReturnInvalidStatusCode(String statusCode) {
        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", 1000)
                        .setParam("http.socket.timeout", 1000));

        log("DELETE request");
        String response = given()
                .spec(requestSpecification).config(config)
                .when()
                .delete(HOST + STATUS_ENDPOINT + statusCode)
                .then()
                .statusCode(400)
                .and().extract().body().htmlPath().getString("body");
        assertEquals(response, "Invalid status code");
    }

    /**
     * Positive case. Should redirect request.
     **/
    @Test
    public static void shouldRedirect() {
        given()
                .spec(requestSpecification)
                .when()
                .get(HOST + HEADERS_ENDPOINT + REDIRECT_ENDPOINT + 1)
                .then()
                .statusCode(302);
    }

    public static void log(String message) {
        Reporter.log(message, true);
    }
}
